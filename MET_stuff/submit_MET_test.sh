#!/bin/bash

#requesting one node

# keep environment variables


#SBATCH -N1 
#requesting cpus

# request enough memory
#SBATCH --mem=150G

#SBATCH --time=48:00:00

#SBATCH --export=ALL

# mail on failures
#SBATCH --mail-user=ruben.honscheid.20@ucl.ac.uk
#SBATCH --mail-type=FAIL

# Change log names; %j gives job id, %x gives job name
#SBATCH --output=/home/xzcaprho/project/salt/MET_stuff/out_test/slurm-%j.%x.out
# optional separate error output file
# #SBATCH --error=/home/xzcaprho/project/salt/MET_stuff/out_test/slurm-%j.%x.err


# Request everything !
#SBATCH -c40
#SBATCH -p LIGHTGPU
#SBATCH --gres=gpu:a100:1
#SBATCH --job-name=gnmet_test


cd /home/xzcaprho/project/salt/
echo "Moved dir, now in:"
pwd

echo $CUDA_VISIBLE_DEVICES

export CUDA_VISIBLE_DEVICES=MIG-4b65de50-b855-56b6-92d6-30e9958cc634



eval "$(/share/apps/anaconda/3-2022.05/bin/conda shell.bash hook)"
conda activate conda/envs/salt

echo "Testing on tt-bar simulated data."
salt test --config /home/xzcaprho/project/salt/MET_stuff/outGNMET_First/gnmet_first_b2000_e40_20240708-T163048/config.yaml --data.test_file /home/xzcaprho/project/MET_Investigation/delphes_ucl_tutorial/to_train/1e7events_ttbar_onlyleptonic_13tev_weighted_test.h5 
conda deactivate

echo "Done."
