import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import h5py

from datetime import datetime
from puma import Histogram, HistogramPlot


def load_data(file):
  '''
  Load the data from the h5 file
  '''

  file = h5py.File(file)
  predictions = file["regression_with_mean_std_norm_gen_MET"]
  truth = file["gen_MET"]
  measurement = file["meas_MET"]

  return predictions, truth, measurement

def make_histo(predictions, truth, measurement, outdir, bins=None):
  '''
  Make the histogram
  '''
    
  plot = HistogramPlot(
    xlabel=variable,
    ylabel="Weighted Number of Events",
    atlas_second_tag="empty",
    bins=bins,
    stacked=True,
    norm=False,
    logy=True,
    y_scale=1.5,
  )

  # Calculate residuals

  residuals_pred = (predictions - truth) / truth
  residuals_meas = (measurement - truth) / truth

  # Create histograms
  hist_pred = Histogram(
    data=residuals_pred,
    weights=weights,
    bins=bins,
    label="Predicted MET",
    color="blue",
  )
  hist_meas = Histogram( 
    data=residuals_meas,
    weights=weights,
    bins=bins,
    label="Measured MET",
    color="red",
  )

  # Add histograms to plot
  plot.add_hist(hist_pred)
  plot.add_hist(hist_meas)

  plot.draw()

  # Generate filename
  now = datetime.now()
  date_time_string = now.strftime("%Y-%m-%d %H:%M:%S")

  plot.savefig(outdir + f"/_histo_{date_time_string}.png")

def main():

  filename = "/home/xzcaprho/project/salt/MET_stuff/outGNMET_First/gnmet_first_b2000_e40_20240708-T163048/ckpts/epoch=009-val_loss=0.06038__test_1e7events_ttbar_onlyleptonic_13tev_weighted_test.h5"
  predictions, truth, measurement = load_data(filename)

  out_path = "/home/xzcaprho/project/salt/MET_stuff/plotting"
  make_histo(predictions, truth, measurement, out_path)


  



if __name__ == "__main__":

  main()
