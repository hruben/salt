import h5py
import matplotlib.pyplot as plt
import numpy as np

filename = "/home/xzcaprho/project/salt/salt/configs/logs/GN2_20240704-T173657/ckpts/epoch=000-val_loss=0.00000__test_1e6events_ttbar_onlyleptonic_13tev_fixed_final.h5"
file = h5py.File(filename, "r")

true_MET = file["event"]["gen_MET"][:]
predicted_MET = file["event"]["regression_with_mean_std_norm_gen_MET"][:]

meas_MET = file["event"]["meas_MET"][:]

# bins = int(np.sqrt(len(true_MET)))

# print("min MET: ", np.min(true_MET)," max MET: ", np.max(true_MET))

delta_predicted_MET = (true_MET - predicted_MET )/ true_MET
delta_measured_MET = (true_MET - meas_MET) / true_MET

print("min delta predicted MET: ", np.min(delta_predicted_MET)," max delta predicted MET: ", np.max(delta_predicted_MET))
print("min delta measured MET: ", np.min(delta_measured_MET)," max delta measured MET: ", np.max(delta_measured_MET))

plt.figure()
plt.hist(delta_predicted_MET, bins=np.linspace(-5,5,100), label="Predicted MET", histtype = 'step')
plt.hist(delta_measured_MET, bins=np.linspace(-5,5,100), label="Measured MET", histtype = 'step')
# plt.hist(true_MET - predicted_MET, bins=100, label="True - Predicted MET", histtype = 'step')
# plt.hist(true_MET - meas_MET, bins=100, label="True - Measured MET", histtype = 'step')
plt.yscale('log')
plt.legend(loc = 'best')
plt.title("MET Residuals")
plt.xlabel('Residual MET [GeV]')
plt.ylabel('Number of Events')
plt.grid(True)
plt.savefig("MET_residuals.png", dpi = 300)