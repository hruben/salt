#!/bin/bash

#requesting one node

# keep environment variables


#SBATCH -N1 
#requesting cpus

# request enough memory
#SBATCH --mem=150G

#SBATCH --time=48:00:00

#SBATCH --export=ALL

# mail on failures
#SBATCH --mail-user=ruben.honscheid.20@ucl.ac.uk
#SBATCH --mail-type=FAIL

# Change log names; %j gives job id, %x gives job name
#SBATCH --output=/home/xzcaprho/project/salt/MET_stuff/out/slurm-%j.%x.out
# optional separate error output file
# #SBATCH --error=/home/xzcaprho/project/salt/MET_stuff/out/slurm-%j.%x.err


# Request everything !
#SBATCH -c40
#SBATCH -p LIGHTGPU
#SBATCH --gres=gpu:a100:1
#SBATCH --job-name=gnmet_first

project=GNMET_First

name_base="gnmet_first"

config="/home/xzcaprho/project/salt/salt/configs/GNMET.yaml"
output_dir="/home/xzcaprho/project/salt/MET_stuff/out"$project
num_devices=1
num_workers=40
raw_batchsize=2000
batch_size=$(($raw_batchsize/$num_devices))
max_epochs=40

name=$name_base"_b"$raw_batchsize"_e"$max_epochs

node=$(hostname)
echo "Running on " $node

cd /home/xzcaprho/project/salt/
echo "Moved dir, now in:"
pwd

echo $CONDA_DEFAULT_ENV

echo "CUDA_VISIBLE_DEVICES:"
echo $CUDA_VISIBLE_DEVICES

nvidia-smi

echo "Ntasks"
echo $SLURM_NTASKS

eval "$(/share/apps/anaconda/3-2022.05/bin/conda shell.bash hook)"
# activate environment
conda activate conda/envs/salt
#source setup/install.sh

cd salt

export CUDA_VISIBLE_DEVICES=MIG-4b65de50-b855-56b6-92d6-30e9958cc634

export COMET_API_KEY=PwRZCswTdemydgIBw6RvxGII5
export COMET_WORKSPACE='rhoenscheid'
export COMET_PROJECT_NAME='salt-'$project
export OMP_NUM_THREADS=1

echo "Running training script..."
echo "Batchsize: "$batch_size
# python main.py -h
# TEST RUN 
# salt  fit --config $config --trainer.fast_dev_run 1 --trainer.devices 1 --data.num_workers 40 --data.batch_size 4000 --trainer.max_epochs 10 --trainer.default_root_dir $output_dir

# 3 GPU, 12k total batchsize, 10 epochs
# salt  fit --config $config --trainer.devices 3 --data.num_workers 120 --data.batch_size 4000 --trainer.max_epochs 10 --trainer.default_root_dir $output_dir

salt  fit --force --config $config --trainer.devices $num_devices --data.num_workers $num_workers \
    --data.batch_size $batch_size --trainer.default_root_dir $output_dir \
    --name $name --trainer.max_epochs $max_epochs \